variable "barbican_project_id" {
  default = "some-project-id"
}

locals {
  ttl_collection_name    = "notifications_log"
  ttl_field              = "expiresAt"
  ttl_manage_script_path = "${abspath(path.module)}/manage_firestore_ttl.sh"
}


resource "null_resource" "random-dependency" {
}

# Adds a TTL policty for the "local.ttl_field" field of the "local.ttl_collection_name" collection.
resource "null_resource" "firestore_ttl_policy" {
  depends_on = [
    null_resource.random-dependency,
  ]

  # When any of these values change:
  # First, the "destroy" provisioner will be called with the old values.
  # Second, the "create" provisioner will be called with new values. 
  triggers = {
    script_path     = local.ttl_manage_script_path
    project_id      = var.barbican_project_id
    collection_name = local.ttl_collection_name
    field           = local.ttl_field

    script_content_changed = md5(file(local.ttl_manage_script_path))
  }

  provisioner "local-exec" {
    when    = create
    command = "/bin/bash ${local.ttl_manage_script_path} create ${var.barbican_project_id} ${local.ttl_collection_name} ${local.ttl_field}"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "/bin/bash ${self.triggers.script_path} destroy ${self.triggers.project_id} ${self.triggers.collection_name} ${self.triggers.field}"
  }
}

output "file_path" {
  value = null_resource.firestore_ttl_policy.triggers.script_path
}

output "file_content_hash" {
  value = null_resource.firestore_ttl_policy.triggers.script_content_changed
}
