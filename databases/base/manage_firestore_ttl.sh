#!/bin/sh

function exit_with_msg() {
    echo $2
    exit $1
}

function create_ttl_policy() {
    exit_with_msg 0 "Created a TTL policy (mock)."
}

function destroy_ttl_policy() {
    exit_with_msg 0 "Destroyed a TTL policy (mock)."
}

echo "Arguments provided: " $1 $2 $3 $4
case $1 in
   "create") 
        create_ttl_policy
        ;;
   "destroy") 
        destroy_ttl_policy 
        ;;
   *) 
        exit_with_msg 1 "Command not found. Available commands: create, destroy."
        ;;
esac

# A comment to change file hash

# A comment to change file hash

# A comment to change file hash

# A comment to change file hash
