init:
	cd databases/dev && terraform init

apply: init
	cd databases/dev && terraform apply

destroy: init
	cd databases/dev && terraform destroy

# Appends a comment to the script to change its hash.
test-script-changed:
	echo -e "\n# A comment to change file hash" >> ./databases/base/manage_firestore_ttl.sh
	cd databases/dev && terraform apply
